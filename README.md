# Wardrobify

Team:

* Antonio - hats
* Person 2 - Which microservice? (walker but separate repo - shoes)

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

The created Hat model has attributes referencing a hat's fabric, the style of the hat, the color of the hat, and a picture of the hat.

Made communication between both microservices to poll location data. This allowed the hats microservice to have access to the Location model attributes inside of a LocationVO.
