from django.urls import path

from .views import (
    api_list_hats, delete_hat_id
)

urlpatterns = [
    path("hats/", api_list_hats, name="api_list_hats"),
    path("hats/<int:pk>/", delete_hat_id, name="delete_hat_id")
]
