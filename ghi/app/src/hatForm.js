import { useEffect, useState } from 'react';

function HatForm() {
    const [hats, setHats] = useState([]);

    const [fabric, setFabric] = useState("");
    const [styleName, setStyleName] = useState("");
    const [color, setColor] = useState("");
    const [pictureUrl, setPictureUrl] = useState("");
    const [location, setLocation] = useState("");

    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
    }

    const handleStyleNameChange = (event) => {
        const value = event.target.value;
        setStyleName(value);
    }

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handlePictureUrlChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value)
    }

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value)
    }

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/hats/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setHats(data.hats);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div>
            test
        </div>
    )
}
